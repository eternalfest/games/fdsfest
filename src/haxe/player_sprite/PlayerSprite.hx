package player_sprite;
import player_sprite.actions.AddPlayerSprite;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.Obfu;

@:build(patchman.Build.di())
class PlayerSprite {

    @:diExport
    public var action(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var max3state: Int = 0;
    public var max3: Dynamic;
	public var max3lunettes: Dynamic;

    public function new() {
        this.action = new AddPlayerSprite(this);

        var patches = [
            Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                this.max3lunettes._xscale = 75;
			    this.max3lunettes._yscale = 75;
			    this.max3lunettes._x = this.max3._x - 11;
			    this.max3lunettes._y = this.max3._y - 28;
			    if (this.max3state == 1) {
			    	this.max3._x += -1.5;
			    	if (this.max3._x < 215) {
			    		this.max3state = 2;
			    		this.max3.animId = self.root.Data.ANIM_PLAYER_STOP.id;
			    		this.max3.gotoAndStop('' + (this.max3.animId + 1));
			    		this.max3.sub.gotoAndStop('12');
			    		this.max3.forceLoop(false);

			    	}
			    }
			    else if (this.max3state == 2) {
				    this.max3.animId = self.root.Data.ANIM_PLAYER_STOP.id;
				    this.max3.gotoAndStop('' + (this.max3.animId + 1));
				    this.max3.sub.gotoAndStop('12');
                }
            }),
        ];
        this.patches = FrozenArray.from(patches);

    }

    public function playerSprite(game: hf.mode.GameMode, s: Int, x: Float, y: Float) {
		this.max3state = s;
		this.max3 = game.world.view.attachSprite("hammer_player", 20 * x, 20 * y + 20, false);
		this.max3._xscale *= -1;
		this.max3lunettes = game.world.view.attachSprite(Obfu.raw("lunettes"), 20 * x, 20 * y, false);

		if (s == 1) {
			this.max3.animId = game.root.Data.ANIM_PLAYER_WALK.id;
			this.max3.gotoAndStop('' + (this.max3.animId + 1));
			this.max3.sub.gotoAndPlay('1');
			this.max3.forceLoop(true);
		}
		else if (s == 2) {
			this.max3.animId = game.root.Data.ANIM_PLAYER_STOP.id;
			this.max3.gotoAndStop('' + (this.max3.animId + 1));
			this.max3.sub.gotoAndStop('12');
		}
    }
}
