package player_sprite.actions;

import merlin.IActionContext;
import etwin.Obfu;
import merlin.IAction;

class AddPlayerSprite implements IAction {
    public var name(default, null): String = Obfu.raw("playerSprite");
    public var isVerbose(default, null): Bool = false;

    private var mod: PlayerSprite;

    public function new(mod: PlayerSprite) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var s: Int = ctx.getInt(Obfu.raw("s"));
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var y: Float = ctx.getFloat(Obfu.raw("y"));

        this.mod.playerSprite(game, s, x, y);

        return false;
    }
}
