import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import better_script.Players;
import better_script.Rand;
import maxmods.VisualEffects;
import maxmods.Misc;
import mc2.Mc2;
import fdsfest.FDSfest;
import player_sprite.PlayerSprite;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	gameParams: GameParams,
	noNextLevel: NoNextLevel,
    players: Players,
    rand: Rand,
    player_sprite: PlayerSprite,
    visualEffects: VisualEffects,
    misc: Misc,
    mc2: Mc2,
    fdsfest: FDSfest,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
