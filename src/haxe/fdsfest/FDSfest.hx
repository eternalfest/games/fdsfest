package fdsfest;
import fdsfest.actions.Ballon;

import merlin.IAction;

@:build(patchman.Build.di())
class FDSfest {

    @:diExport
    public var action(default, null): IAction;

    public function new() {
        this.action = new Ballon(this);
    }

    public function createBallon(game: hf.mode.GameMode, x: Float, y: Float) {
        var ballon = game.root.entity.bomb.player.SoccerBall.attach(game, x, y);
        game.fxMan.attachShine(ballon.x, ballon.y);
    }
}
