package fdsfest.actions;

import merlin.IActionContext;
import etwin.Obfu;
import merlin.IAction;

class Ballon implements IAction {
    public var name(default, null): String = Obfu.raw("ballon");
    public var isVerbose(default, null): Bool = false;

    private var mod: FDSfest;

    public function new(mod: FDSfest) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();

        var x: Float = game.flipCoordReal(ctx.getHf().Entity.x_ctr(ctx.getFloat(Obfu.raw("x"))));
        var y: Float = ctx.getHf().Entity.y_ctr(ctx.getFloat(Obfu.raw("y")));

        this.mod.createBallon(game, x, y);

        return false;
    }
}
